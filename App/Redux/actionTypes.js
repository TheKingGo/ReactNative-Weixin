export default {
  LoginStart: 'loginStart', // 定义统一的type
  LoginSuccess: 'loginSuccess',
  LoginFail: 'loginFail',
  LoginOut: 'LoginOut',
  RegisterStart: 'RegisterStart',
  RegisterSuccess: 'RegisterSuccess',
  RegisterFail: 'RegisterFail',
  GetFriend: 'GetFriend',
  SetMessage: 'SetMessage',
  SetTalkList: 'SetTalkList',
  DeleteTalkList: 'DeleteTalkList',
  AddLastMessage: 'AddLastMessage',
  AddUnReadMessage: 'AddUnReadMessage',
  DeleteUnReadMessage: 'DeleteUnReadMessage',
  ChangeShowButton: 'ChangeShowButton',
  ChangeShowInput: 'ChangeShowInput',
  UpdateUser: 'UpdateUser',
}
